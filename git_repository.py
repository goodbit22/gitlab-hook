from git import Repo, exc
from session_gitlab import Session
from logging import info, error, warning, INFO, getLogger
from re import search
from pathlib import Path
from gitlab import GitlabCreateError

group_repo="3arts" 

class GitRepository:
    def __init__(self, path: str):
        self.path = path
        self.default_branch_name='master'
    
    def create_branch(self, new_branch_name: str):
        logger = getLogger()
        logger.setLevel(INFO)
        directory_repo_path = self.path.replace('/gradlew','') if self.path.find('gradlew') != -1 else self.path
        info("A branch will be created in the {0} directory".format(directory_repo_path))
        repo = Repo(directory_repo_path)
        try:
            repo.git.rev_parse('--verify','--quiet', new_branch_name)
            error("Branch {0} already exists".format(new_branch_name))
            return False
        except exc.GitCommandError:
            pass
        try:
            repo.create_head(new_branch_name)
            repo.git.checkout(new_branch_name)    
            info("Switch on {0} branch".format(new_branch_name))
            return True
        except exc.GitCommandError:
            error("{0} Branch  has not been created".format(new_branch_name))
            return False

    def commitAndPush(self, new_branch_name: str, file_to_commit, commit_message):
        logger = getLogger()
        logger.setLevel(INFO)
        repo = Repo(self.path)
        repo.index.add(file_to_commit)
        info("Added {0} file".format(file_to_commit))
        repo.index.commit(commit_message)
        info("Commit about '{0}' content was created".format(commit_message))
        repo.git.push("--set-upstream","origin", new_branch_name)
        info("The push has been made")
        
    def __get_branch_name(self, project_name: str) -> str:
        path_to_file='default_branch_config.txt'
        path = Path(path_to_file)
        if path.is_file():
            with open(path_to_file, 'r') as file_branch_config:
                repo_branch = [line for line in file_branch_config] 
            for repo in repo_branch:
                pr,branch_name = repo.split(':')
                if pr == project_name:
                    return branch_name
            return self.default_branch_name
        else:
            error(f'The file {path_to_file} does not exist')
            return self.default_branch_name

    def mergeRequest(self, branch_name: str, commit_message: str):
        logger = getLogger()
        logger.setLevel(INFO)
        root_path="repo_3arts"
        pattern=r"(?P<name>{})(?P<name_project>\S*/)".format(root_path)
        result = search(pattern, self.path)
        project_name= group_repo + "/" + str(result.group(2)).replace("/","")
        target_branch=self.__get_branch_name(project_name)
        if self.path.find('gradlew') != -1:    
            directory_repo_path=self.path.replace('/gradlew','')
        else:   
            directory_repo_path=self.path
        repo = Repo(directory_repo_path)
        session = Session()
        gl =session.get_session_gitlab()
        project = gl.projects.get(project_name)
        try:
            mr = project.mergerequests.create({'source_branch': branch_name,
                                   'target_branch': target_branch,
                                   'title': 'merge branch ' + commit_message
                                   }) 
        except GitlabCreateError as create_error:
            error(f"Creating merge request {branch_name} failed: {create_error}")
        if getattr(mr,'merge_error'):
            error(f"Creating merge request {branch_name} failed")
        else:
            info(f"Creating merge request for {branch_name} to master was successful")
        repo.git.checkout(target_branch)
        info("Switch to {0} branch".format(target_branch))

