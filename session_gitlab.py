import gitlab
from os import environ


class Session:
    def __init__(self):
        repo_gitlab = "http://gitlab-3arts.krakow.comarch"
        api_gitlab = repo_gitlab + "/api/v4/projects"
        self.token = environ.get("Token")
        self.gl = gitlab.Gitlab(repo_gitlab, private_token=self.token)

    def get_session_gitlab(self):
        return self.gl
