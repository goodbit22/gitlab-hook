import xml.etree.ElementTree as xml
from re import  match
from git_repository import GitRepository
from logging import INFO, getLogger, warning, info

def find_relativePath(root, namespaces):
    logger = getLogger()
    logger.setLevel(INFO)
    root_tag = root.findall(".//xmlns:parent", namespaces=namespaces)
    for child in root_tag:
        relativePath = child.find("xmlns:relativePath", namespaces=namespaces)
        if relativePath.text is None:
            warning("Tag <relativePath> jest pusty")
            return None
        else:
            info("Tag <relativePath> nie jest pusty")
            return relativePath.text

def upgrade_repo(n_groupId, n_artifactId, n_version, version, current_path, path_pom, pom):
    logger = getLogger()
    logger.setLevel(INFO)
    branch_name = "upgrade_" + n_groupId +"_" + n_artifactId + "_from_" + version.text + "_to_" + n_version
    gitrepository =  GitRepository(current_path + '/' + path_pom)
    gitrepository.create_branch(branch_name)
    info("podmieniono wersje z {0} na {1}".format(version.text , n_version))
    version.text = n_version
    pom.write('pom.xml', xml_declaration = True, encoding = 'UTF-8', method = 'xml')
    message='TARTS-15204 automatic version update in {0} file'.format('pom.xml')
    gitrepository.commit_and_push(branch_name, path_pom, message)
    gitrepository.create_merge_request(branch_name, message)

def variant_without_version(root, namespaces, pom, n_groupId, n_artifactId, n_version, current_path, path_pom):
            logger = getLogger()
            logger.setLevel(INFO)
            relativePath_text = find_relativePath(root, namespaces)
            if relativePath_text is not None:
                print(relativePath_text)
                parent_pom = xml.parse(relativePath_text)
                namespaces = {'xmlns': 'http://maven.apache.org/POM/4.0.0'}
                parent_root = parent_pom.getroot()
                parent_root_children = parent_root.findall(".//xmlns:dependency", namespaces=namespaces)
                for parent_root_child in parent_root_children:
                    groupId = parent_root_child.find("xmlns:groupId", namespaces=namespaces)
                    artifactId = parent_root_child.find("xmlns:artifactId", namespaces=namespaces)
                    version    = parent_root_child.find("xmlns:version", namespaces=namespaces)
                    if groupId.text == n_groupId and artifactId.text ==  n_artifactId:
                        upgrade_repo(n_groupId, n_artifactId, n_version, version, current_path, path_pom, pom )

def variant_with_a_variable_in_tag_version(root, namespaces, pom, version, n_groupId, n_artifactId, n_version, current_path, path_pom):
    logger = getLogger()
    logger.setLevel(INFO)
    info("znaleziono zmienna w tagu <version>")
    properties_tag = root.findall(".//xmlns:properties", namespaces=namespaces)
    for child in properties_tag:
        version_tag = child.find("xmlns:" + str(version), namespaces=namespaces)
        if version_tag.text is None:
            warning("Tag <{0}> is empty".format(str(version)))
        else:
            upgrade_repo(n_groupId, n_artifactId, n_version, version, current_path, path_pom, pom )

def variant_with_normal_version(pom, n_groupId, n_artifactId, n_version, version, current_path, path_pom):
    upgrade_repo(n_groupId, n_artifactId, n_version, version, current_path, path_pom, pom )

def find_dependency_in_pom_file(dependency_search, n_groupId, n_artifactId, n_version, current_path, path_pom):
    print("Found ", dependency_search)
    xml.register_namespace('', 'http://maven.apache.org/POM/4.0.0')
    pom_file = xml.parse('pom.xml')
    namespaces = {'xmlns': 'http://maven.apache.org/POM/4.0.0'}
    root_element = pom_file.getroot()
    dependencies = root_element.findall(".//xmlns:dependency", namespaces=namespaces)
    for d in dependencies:
        groupId = d.find("xmlns:groupId", namespaces=namespaces)
        artifactId = d.find("xmlns:artifactId", namespaces=namespaces)
        version    = d.find("xmlns:version", namespaces=namespaces)
        if groupId.text == n_groupId and artifactId.text ==  n_artifactId:
            version_defined_as_variable_regex='r"^[$]{\S*}$"'
            if version is None: 
                variant_without_version(root_element, namespaces, pom_file, n_groupId, n_artifactId, n_version, current_path, path_pom)
            #elif match("^[$]{\S*}$",str(version)):
            elif match(version_defined_as_variable_regex, str(version)):
                variant_with_a_variable_in_tag_version(root_element, namespaces, pom_file, version, n_groupId, n_artifactId, n_version, current_path, path_pom)
            else:
                variant_with_normal_version(pom_file, n_groupId, n_artifactId, n_version, version, current_path, path_pom)
        if version is None: 
            print(groupId.text + '\t' + artifactId.text)
        else:
            print(groupId.text + '\t' +  artifactId.text + '\t' + version.text)

def are_dependencies_in_the_pom_file(format_dep, current_path, path_pom, date_sent):
    project_name_send_by_api=date_sent["project_name"]
    dependency_api_groupId = date_sent["groupId"]
    dependency_api_artifactId = date_sent["artificatId"]
    dependency_api_version = date_sent["new_version"]
    without_version = dependency_api_version
    print(format_dep)
    print(dependency_api_version)
    print(without_version)
    dependency_search = next((x for x in format_dep if x.find(without_version) != -1 ), '')
    if dependency_search in '':
        warning("nothing found")
    else:
        find_dependency_in_pom_file(dependency_search, dependency_api_groupId, dependency_api_artifactId,  dependency_api_version, current_path, path_pom)