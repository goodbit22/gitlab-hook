from os import path
from re import sub, findall, match, search
from git_repository import GitRepository
from logging import warning, error, info, getLogger, INFO
import subprocess


def read_build_gradle(build_gradle, build_gradle_version, dependency_api_group_id, dependency_api_artifact_id, dependency_api_version, full_path_gradlew):
    def write_modification_to_file(replacement):
        with open(build_gradle, 'w') as filename_gradle_write:
            filename_gradle_write.write(replacement)

    with open(build_gradle, 'r') as filename_gradle_read:
        lines_dep = [True for f in  filename_gradle_read.read() if search("dependencies", f) ] 
        w = filename_gradle_read.read()

    replacement = ""
    for current_line in w.split('\n'):
        if match(r"[$]{\S*}$", build_gradle_version):
            print("Gradle file has a version in the variable")
            dependency_version_expression = sub(r"[$,{}]", '',build_gradle_version)
            if search(dependency_version_expression, current_line):
                line_to_change = sub(r" " , '',current_line)
                line_to_change = line_to_change.strip('""')
                version_dependency_gradle_file = current_line.replace(dependency_version_expression + "=",  line_to_change)
                print(version_dependency_gradle_file)
                if version_dependency_gradle_file == dependency_api_version:
                    info("The dependency version is the same")
                else:
                    print("The dependency version is other")
                    print(version_dependency_gradle_file)
                    line_to_change = current_line.replace(version_dependency_gradle_file, dependency_api_version)
                    current_line = line_to_change
                    print(line_to_change)
        if findall(dependency_api_group_id, current_line) and findall(dependency_api_artifact_id, current_line):
            print(f"contents of the line: {current_line}")
            if build_gradle_version == "":
                print("There is no version")
            else:
                print("has versions in normal form")
                if build_gradle_version ==  dependency_api_version:
                    print("dependency version is the same")
                else:
                    print("the dependency version is different")
                    line_to_change =  current_line.replace(build_gradle_version, dependency_api_version )
                    print(line_to_change)
                    current_line = line_to_change
        replacement = replacement + current_line + "\n"
    branch_name = "upgrade_" + dependency_api_group_id +"_" + dependency_api_artifact_id + "_from_" +build_gradle_version + "_to_" + dependency_api_version
    gitrepository =  GitRepository(full_path_gradlew)
    gitrepository.create_branch(branch_name)    
    write_modification_to_file(replacement)   
    message='TARTS-15204 automatic version update in {0} file'.format(build_gradle)
    gitrepository.commit_and_push(branch_name, build_gradle, message)
    gitrepository.create_merge_request(branch_name, message)

def run_gradlew():
    process  = subprocess.Popen(['./gradlew', 'dependencies', '--configuration', 'implementation'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT, universal_newlines=True)
    stdout, _ = process.communicate()
    exit_code = process.wait()
    return stdout, exit_code

def return_dependency(dependency_api_group_id, dependency_api_artifact_id, stdout, filename_gradle, filename_gradle_kotlin, dependency_api_version,
full_path_gradlew):
    dependency = findall(rf"{dependency_api_group_id}:{dependency_api_artifact_id}\S*", str(stdout))
    if dependency:
        logger = getLogger()
        logger.setLevel(INFO)
        info(f"Dependency found: {dependency} ")
        dependency_split = str(dependency).split(':')
        build_gradle_version = ""
        if len(dependency_split) == 3:
            build_gradle_version = str(dependency_split[2]).replace("']", "")
        else:
            info("there is no version in the build.gradle file {0}".format(build_gradle_version))
        filename_gr =  filename_gradle if path.isfile(filename_gradle) else filename_gradle_kotlin
        read_build_gradle(filename_gr,  build_gradle_version, dependency_api_group_id, dependency_api_artifact_id, dependency_api_version, full_path_gradlew)
    else:
        print("no dependency found")

def settings_file_doesnt_exist(filename_gradle, filename_gradle_kotlin,  full_path_gradlew, dependency_api_group_id, dependency_api_artifact_id, dependency_api_version):
    print("plik gradlew nie ma settings")
    if path.isfile(filename_gradle) or path.isfile(filename_gradle_kotlin):
        stdout, exit_code = run_gradlew()
        if exit_code == 1:
            error(f"Problem with gradlew file in {full_path_gradlew} location")
            return 1
        return_dependency(dependency_api_group_id, dependency_api_artifact_id, stdout, filename_gradle, filename_gradle_kotlin, dependency_api_version,
        full_path_gradlew)

def there_are_not_some_modules_in_the_settings_file(filename_gradle,  filename_gradle_kotlin, full_path_gradlew, dependency_api_group_id, 
    dependency_api_artifact_id, dependency_api_version ):
    print("nie ma modulow")
    stdout, exit_code = run_gradlew()
    if exit_code == 1:
        error("Problem with gradlew file in {0} location".format(full_path_gradlew))
        return 1
    return_dependency(dependency_api_group_id, dependency_api_artifact_id, stdout, filename_gradle, filename_gradle_kotlin, dependency_api_version,
    full_path_gradlew)

def run_gradlew_with_dependencies(inc):
    process  = subprocess.Popen([ './gradlew', ":" + inc + ':dependencies', '--configuration', 'implementation'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT, universal_newlines=True)
    stdout, _ = process.communicate()
    exit_code = process.wait()
    return stdout, exit_code


def there_are_some_modules_in_the_settings_file(filename_gradle, filename_gradle_kotlin, full_path_gradlew, 
    dependency_api_group_id, dependency_api_artifact_id, includes, dependency_api_version):
    info("has modules")
    for inc in includes:
        print(inc)
        stdout, exit_code = run_gradlew_with_dependencies(inc)
        if exit_code == 1:
            error(f"Problem with gradlew file in {full_path_gradlew} location")
            return 1
        return_dependency(dependency_api_group_id, dependency_api_artifact_id, stdout, filename_gradle, filename_gradle_kotlin, dependency_api_version,
        full_path_gradlew)