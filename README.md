# gitlab-hook-comarch-sbss

My gitlab hook written when I worked at Comarch.

The hook tracks version changes in the sbss libraries.

The solution will consist of two parts:

1. A git hook that can catch artifact version changes in maven/gradle based projects.
2. An application which, when a version change of an artefact is reported, will track its use in the 3arts modules and make a merge request with the version boost

## Table of Contents

- [gitlab-hook-comarch-sbss](#gitlab-hook-comarch-sbss)
  - [Table of Contents](#table-of-contents)
  - [Technologies](#technologies)
  - [Installation](#installation)
  - [Running](#running)
  - [Authors](#authors)
  - [License](#license)

## Technologies

- flask
- python-gitlab
- GitPython
- aiohttp
- httpx
- "Flask[async]"
- pytest

## Installation

1. `virtualenv -p python3 hook`
2. `source .hook/bin/activate`
3. `pip3 install -r requirements.txt`

## Running
  `python3 api_rest.py`

## Authors

***
__goodbit22__ --> 'https://gitlab.com/users/goodbit22'
***

## License

  Apache License Version 2.0
