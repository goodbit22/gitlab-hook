from git import Repo, exc
from session_gitlab import Session
from os import path, mkdir
from pom_config import find_pom_files, pom_content_dependency
from gradle_config import find_gradle_files, gradle_content_dependency
from logging import info, error, INFO, getLogger, warning

class Application:
    def __init__(self, directory='repo_3arts'):
        session = Session()
        self.gl =session.get_session_gitlab()
        self.directory = directory
        self.skip_repositories=[f"{self.directory}/migration"]
    
    def __get_repository_names(self):
        return [project.attributes['name'] for project in self.gl.projects.list(all=True)]

    def __get_repository_urls_and_names(self):
        return {project.attributes['name']: project.attributes['ssh_url_to_repo'] for project in self.gl.projects.list(all=True)}

    def __get_merge_requests(self):
        mrs = self.gl.mergerequests.list(scope="all")[0]
        project = self.gl.projects.get(mrs.project_id, lazy=True)
        editable_mr = project.mergerequests.get(mrs.iid, lazy=True)
        print(project)
        print(editable_mr)

    def __create_directory_for_repositories(self):
        logger = getLogger()
        logger.setLevel(INFO)
        if path.isdir(self.directory):
            info(f"{self.directory} directory already exists")
        else:
            mkdir(self.directory)
            info(f"{self.directory} directory has been created")
    
    def __clone_repositories(self):
        repo_names_and_repo_urls= self.__get_repository_urls_and_names()
        logger = getLogger()
        logger.setLevel(INFO)
        info("\nGit clone")
        for repository_name, ssh_url_to_repo in repo_names_and_repo_urls.items():
            try:
                full_repository_path = self.directory + "/" + repository_name
                if not path.isdir(full_repository_path): 
                    Repo.clone_from(ssh_url_to_repo, full_repository_path)
                warning(f"The repository {repository_name} has already been cloned")
            except exc.GitError:
                error(f"The cloning operation failed on the {full_repository_path} directory")


    def __pull_repositories(self):
        name_repositories = self.__get_repository_names()
        logger = getLogger()
        logger.setLevel(INFO)
        info("\nGit pull")
        for name_repo in name_repositories: 
            full_repo_path = self.directory + "/" + name_repo
            try:
                for skip_repository in self.skip_repositories:
                    if full_repo_path == skip_repository: pass
                    else:
                        info(f"Pull of the {full_repo_path} repository is in progress")
                        Repo(full_repo_path).remotes.origin.pull()
                        info(f"Pull operation on the {full_repo_path} directory was successful")
            except exc.GitError:
                error(f"Pull operation failed on the {full_repo_path} directory")

    def __find_files(self):
        name_repositories = self.__get_repository_names()
        pom_files = []
        gradle_files = {}
        for name_repo in  name_repositories: 
            full_repo_path = self.directory + "/" + name_repo
            pom_file = find_pom_files(full_repo_path)
            if pom_file:
                pom_files.append(pom_file)
            gradle_file  = find_gradle_files(full_repo_path)
            if gradle_file:
                for repo_gradlew, repo_setting in gradle_file.items():
                    gradle_files[repo_gradlew] = repo_setting
        print(gradle_files)
        return pom_files, gradle_files
    
    def __search_for_dependency(self, date_sent):
        pom_files, gradle_files = self.__find_files()
        gradle_content_dependency(gradle_files, date_sent)
        pom_content_dependency(pom_files, date_sent)

    def process_hook_scan_repositories(self, date_sent):
        logger = getLogger()
        logger.setLevel(INFO)
        info("Start scan repositories")
        self.__create_directory_for_repositories()
        self.__clone_repositories()
        self.__pull_repositories()
        self.__search_for_dependency(date_sent)
        info("Stop scan repositories")
