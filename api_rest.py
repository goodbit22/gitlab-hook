from flask import Flask, jsonify, request
from main import Application
from logging import info, INFO, getLogger
import asyncio
import threading, queue

api = Flask(__name__)
jobs = queue.Queue(100)


def do_stuff():
    while True:
        value = jobs.get()
        app = Application()
        app.process_hook_scan_repositories(value)
        print("The end")
        jobs.task_done()


@api.route("/dependencies", methods=["POST"])
async def dependencies():
    dependency_info = request.get_json()
    jobs.put(dependency_info)
    print("waiting for queue to complete", jobs.qsize(), "tasks")
    print("all done")
    return jsonify({"ok": dependency_info}), 201


if __name__ == "__main__":
    worker = threading.Thread(target=do_stuff)
    worker.start()
    api.run(debug=False)
