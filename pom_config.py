from distutils.log import error
from os import walk, chdir, getcwd, path
from pom_dependencies import are_dependencies_in_the_pom_file
import subprocess
from re import findall


def find_pom_files(path_p):
    result = []
    filename_pom = "pom.xml"
    print(f"\n The {path_p} catalogue search in progress {filename_pom} files \n")
    for root, dirs, files in walk(path_p):
        for name in files:
            if name == filename_pom:
                path_pom_file = path.join(root, name)
                print(path_pom_file)
                result.append(path_pom_file)
    return result


def run_mvn():
    process = subprocess.Popen(
        ["mvn", "dependency:tree"],
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
        universal_newlines=True,
    )
    stdout, _ = process.communicate()
    exit_code = process.wait()
    dep = findall(r"\S\s\+-\s\S*", str(stdout))
    format_dep = [w.replace("] +- ", "") for w in dep]
    return format_dep, exit_code


def pom_content_dependency(pom_files, date_sent):
    current_path = getcwd()
    for pom_files_in_repository in pom_files:
        for pom_file in pom_files_in_repository:
            path_pom = pom_file.replace("/pom.xml", "")
            chdir(current_path + "/" + path_pom)
            format_dep, exit_code = run_mvn()
            if exit_code == 1:
                error(
                    f"There is no dependency module in the pom.xml file for the {path_pom} directory"
                )
                return 1
            else:
                are_dependencies_in_the_pom_file(
                    format_dep, current_path, path_pom, date_sent
                )
                chdir(current_path)
