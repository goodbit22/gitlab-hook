from os import walk, chdir, chmod, getcwd, path, listdir
from re import sub, match
from logging import info, INFO, getLogger
from gradle_dependencies import settings_file_doesnt_exist, there_are_not_some_modules_in_the_settings_file, there_are_some_modules_in_the_settings_file

def find_gradle_files(path_g):
    gradle_files = {}
    filename_setting, filename_settings_kotlin, filename_gradlew = 'settings.gradle', 'settings.gradle.kts', 'gradlew'
    print(f"Search is in progress in the {path_g} directory for files {filename_gradlew}, {filename_setting} and {filename_settings_kotlin}\n")
    for root, dirs, files in walk(path_g):
        for name in files:
            if name == filename_gradlew:
                path_gradlew_location_file = path.join(root, name)
                path_gradlew_location = str(path_gradlew_location_file.replace('/gradlew', ''))
                gradle_files[path_gradlew_location_file] = "" 
                for list_file in listdir(path_gradlew_location):
                    if list_file == filename_setting or list_file == filename_settings_kotlin:
                        file_setting  = path_gradlew_location + "/" + list_file
                        gradle_files[path_gradlew_location_file] = file_setting
                        break
    return gradle_files            
        

def gradle_content_dependency(repositories_gradle_files, dependencyChangedEvent):
    
    def get_subprojects(setting_gradle):
        with open(setting_gradle, 'r') as setting_gradle_file:
           content_setting_gradle = setting_gradle_file.read().splitlines()
        incl = [ sub(r"^include " , '', s.replace("'", "")) for s in content_setting_gradle if match(r"^include ", s)  ]
        return incl

    current_path = getcwd()
    filename_gradle, filename_gradle_kotlin = 'build.gradle', 'build.gradle.kts'
    print(current_path)
    for gradlew_file, settings_file in repositories_gradle_files.items():
        print(gradlew_file, settings_file)
        project_name_send_by_api=dependencyChangedEvent["project_name"]
        dependency_api_group_id = dependencyChangedEvent["groupId"]
        dependency_api_artifact_id = dependencyChangedEvent["artificatId"]
        dependency_api_version =  dependencyChangedEvent["new_version"]
        full_path_gradlew_location = current_path + '/' + gradlew_file
        print(full_path_gradlew_location)
        chmod(full_path_gradlew_location,  0o755)
        chdir(full_path_gradlew_location.replace('/gradlew', '' ))
        if settings_file:
            print("settings file exist")
            full_path_settings_file =  current_path + '/' + settings_file
            includes = get_subprojects(full_path_settings_file)
            if includes:
                there_are_some_modules_in_the_settings_file(filename_gradle, filename_gradle_kotlin, full_path_gradlew_location, 
                    dependency_api_group_id, dependency_api_artifact_id, includes, dependency_api_version)
            else:
                there_are_not_some_modules_in_the_settings_file(filename_gradle,  filename_gradle_kotlin, full_path_gradlew_location, 
                    dependency_api_group_id, dependency_api_artifact_id, dependency_api_version )
        else:
            settings_file_doesnt_exist(filename_gradle, filename_gradle_kotlin, full_path_gradlew_location, dependency_api_group_id, dependency_api_artifact_id, dependency_api_version)
        chdir(current_path)     