#!/usr/bin/env python
from git import Repo
from xml.etree import ElementTree
from re import search
from logging import info, error, critical, INFO, getLogger
import asyncio
import aiohttp


async def send_request_post_to_api(
    ver_new, groupId, artifactId, project_name, filename_api_conf="api_config.txt"
):
    dependencyChangedEvent = {
        "project_name": project_name,
        "groupId": groupId,
        "artificatId": artifactId,
        "new_version": ver_new,
    }

    def read_api_conf():
        try:
            with open(filename_api_conf, "r") as api_conf_read:
                for conf in api_conf_read.readlines():
                    address = conf
            return address
        except FileNotFoundError:
            error("No such file or directory: {0}".format(filename_api_conf))

    address = read_api_conf()
    try:
        async with aiohttp.ClientSession() as session:
            async with session.post(address, json=dependencyChangedEvent) as response:
                if response.status == 201:
                    info(
                        "The request to api was successful {0}".format(response.status)
                    )
                    print(await response.text())
                else:
                    error(f"The request to api failed {response.status}")
    except aiohttp.client_exceptions.ClientConnectorError as err:
        critical("Problem with api: {0}".format(err))


def is_pom_file(changed_file_path):
    logger = getLogger()
    logger.setLevel(INFO)
    if changed_file_path.find("pom.xml") != -1:
        info("The {0} file is pom.mxl  ".format(changed_file_path))
        return True
    else:
        info("The {0} file is not pom.xml ".format(changed_file_path))
        return False


def check_version_file_pom_and_gradle(
    repo, change_file, last_commit_file, before_last_commit_file, type_file
):
    logger = getLogger()
    logger.setLevel(INFO)
    current_file_content = repo.git.show("%s:%s" % (last_commit_file, change_file))
    previous_file_content = repo.git.show(
        "%s:%s" % (before_last_commit_file, change_file)
    )
    if type_file == "pom":
        ver_new, groupId, artifactId = pull_version_from_pom_file(current_file_content)
        ver_old, groupId, artifactId = pull_version_from_pom_file(previous_file_content)
    else:
        ver_new, groupId, artifactId = pull_version_from_gradle_file(
            current_file_content
        )
        ver_old, groupId, artifactId = pull_version_from_gradle_file(
            previous_file_content
        )
    if ver_new != ver_old:
        info("Version file {0} has been changed".format(type_file))
        info("The version was changed from {0} to {1}".format(ver_old, ver_new))
        get_project_name = print_repository_info(repo)
        asyncio.run(
            send_request_post_to_api(ver_new, groupId, artifactId, get_project_name)
        )
    else:
        info("Version file {0} has not been changed".format(type_file))


def pull_version_from_pom_file(parser_string):
    ns = "http://maven.apache.org/POM/4.0.0"
    ver = ElementTree.fromstring(parser_string).find("{%s}version" % ns)
    groupId = ElementTree.fromstring(parser_string).find("{%s}groupId" % ns)
    artifactId = ElementTree.fromstring(parser_string).find("{%s}artifactId" % ns)
    return ver.text, groupId.text, artifactId.text


def pull_version_from_gradle_file(parser_string):
    version_gradle_file = search(r"\s*version\s*=\s*\'(\d.*)\'", parser_string)
    group_gradle_file = search(r"\s*group\s*=\s*\'(\S*)\'", parser_string)
    name_gradle_file = search(r"\s*name\s*=\s*\'(\S*)\'", parser_string)
    return (
        version_gradle_file.group(1),
        group_gradle_file.group(1),
        name_gradle_file.group(1),
    )


def is_gradle_file(changed_file_path):
    logger = getLogger()
    logger.setLevel(INFO)
    if (
        changed_file_path.find("build.gradle") != -1
        or changed_file_path.find("build.gradle.kts") != -1
    ):
        info(
            "The {0} file is build.gradle or build.gradle.kts ".format(
                changed_file_path
            )
        )
        return True
    info(
        "The {0} file is not gradle.build and build.gradle.kts".format(
            changed_file_path
        )
    )
    return False


def return_last_commit(repo):
    commits = list(repo.iter_commits())
    return commits[0]


def return_last_commit_file(repo, correct_files):
    commits = repo.git.log("--pretty=%H", "--follow", "--", correct_files)
    list_commits_file = [commit for commit in commits.split()]
    if len(list_commits_file) >= 2:
        return list_commits_file[0], list_commits_file[1]
    else:
        return list_commits_file[0], list_commits_file[0]


def print_commit_dependencyChangedEvent(commit):
    print("-----")
    print(str(commit.hexsha))
    print(
        '"{}" by {} ({})'.format(
            commit.summary, commit.author.name, commit.author.email
        )
    )
    print(str(commit.authored_datetime))
    print(str(f"count: {commit.count()} and size: {commit.size}"))
    print(str(f"{commit.summary}"))
    print(str(f"{commit.committer}"))
    print(str(f"{commit.stats.files}"))


def print_repository_info(repo):
    pattern = r":(\S*).git"
    project_name = search(pattern, str((repo[-1].remotes).url))
    return project_name.group(1)


if __name__ == "__main__":
    path_repo = ""
    repo = Repo(path_repo)
    print_repository_info(repo)
    last_commit = return_last_commit(repo)
    changed_files = [
        changed_file_path for changed_file_path, val in last_commit.stats.files.items()
    ]
    for changed_file_path in changed_files:
        if is_pom_file(changed_file_path) == True:
            last_commit_file, before_last_commit_file = return_last_commit_file(
                repo, changed_file_path
            )
            check_version_file_pom_and_gradle(
                repo,
                changed_file_path,
                last_commit_file,
                before_last_commit_file,
                "pom",
            )
        elif is_gradle_file(changed_file_path) == True:
            last_commit_file, before_last_commit_file = return_last_commit_file(
                repo, changed_file_path
            )
            check_version_file_pom_and_gradle(
                repo,
                changed_file_path,
                last_commit_file,
                before_last_commit_file,
                "gradle.build",
            )
