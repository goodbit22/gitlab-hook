import pytest
from unittest import mock, TestCase
from unittest.mock import patch

from requests.sessions import session
from main import Application 
from session_gitlab import Session

class TestApplication(TestCase):
    def test(self):
        # given
        self.dependency_info = {
            "project_name": 'Plugin Nagios',
            "groupId": 'com.comarch.telco.3arts',
            "artificatId": 'sbss-parent',
            "new_version": '1.7'
        }
        #when
        app = Application()
        app.process_hook_scan_repositories(self.dependency_info)
    
    def testGitlab(self):
        with patch('session_gitlab.Session', autospec=True) as MockSession:
            session = Session()
            www = session.get_session_gitlab()
            assert www != None
            


